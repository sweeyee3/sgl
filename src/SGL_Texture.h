#ifndef SGL_TEXTURE_H
#define SGL_TEXTURE_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <string>
#include <vector>
#include "SDL_FontCache.h"
using namespace std;

//SGL Texture wrapper class
class LTexture
{
    public:
        //Initializes variables
        LTexture() 
		{
			// Initialize
			mTexture = NULL;
			mSpritesheetWidth = 0;
			mSpritesheetHeight = 0;
			mPosition.x = 0;
			mPosition.y = 0;			
		};

		// Copy constructor 
    	LTexture(LTexture* tex) 
		{
			mTexture = tex->mTexture;
			mname = tex->mname;

			mtype = tex->mtype; // texture type					
			mSpritesheetWidth = tex->mSpritesheetWidth; // sprite sheet width 
			mSpritesheetHeight = tex->mSpritesheetHeight;	// sprite sheet height
			mClipWidth = tex->mClipWidth; // clip width
			mClipHeight = tex->mClipHeight; // clip height
			mPosition = tex->mPosition; // position

			mCurrentClipIndex = tex->mCurrentClipIndex; // current animation clip index
			mSpriteClips = tex->mSpriteClips; // sprite clips

			mPivotPosition.x = mClipWidth / 2;
			mPivotPosition.y = mClipHeight / 2;

			renderer = tex->renderer;
		}

        //Deallocates memory
        ~LTexture()
		{
			// deallocate
			free();
		};
		enum TextureType
		{
			PNG,
			TTF
		};

		string mname; // name		
		TextureType mtype; // texture type		
        SDL_Texture* mTexture; // texture reference		
        int mSpritesheetWidth; // sprite sheet width 
        int mSpritesheetHeight;	// sprite sheet height
		int mClipWidth; // clip width
		int mClipHeight; // clip height
		SDL_Point mPosition; // position

		int mCurrentClipIndex; // current animation clip index
		vector<SDL_Rect> mSpriteClips; // sprite clips		
		SDL_Renderer* renderer;

		//Creates image from font string
        bool loadFromRenderedText( SDL_Renderer* sdl_renderer, TTF_Font* font, string font_path, int index, string textureText, SDL_Color textColor );
		bool loadFromRenderedText( SDL_Renderer* sdl_renderer, TTF_Font* font, string font_path, int index, int textureText, SDL_Color textColor );
		bool loadFromRenderedText( SDL_Renderer* sdl_renderer, TTF_Font* font, string font_path, int index, double textureText, SDL_Color textColor );
		bool loadFromRenderedText( SDL_Renderer* sdl_renderer, TTF_Font* font, string font_path, int index, float textureText, SDL_Color textColor );        

		// serialize / deserialize
		string serialize();
		void deserialize(vector<string> metaInfo, SDL_Texture* texture, SDL_Renderer* sdl_renderer);		

		void update();
		void fixedUpdate();                
		void render( double angle = 0.0, SDL_RendererFlip flip = SDL_FLIP_NONE, bool update_animation = false );								

		//Deallocates texture
        void free();

		// debug
		void debug_update();
		void debug_imgui_update();
		void debug_render(FC_Font* font);		

    private:		
		// positions		
		SDL_Point mPivotPosition;						
};
#endif