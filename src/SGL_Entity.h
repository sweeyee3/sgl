#ifndef SGL_ENTITY_H
#define SGL_ENTITY_H

#include <SDL2/SDL.h>
#include <string>
#include <map>

#include "SGL_Texture.h"
#include "SGL_Bound.h"
#include "SGL_Transform.h"
#include "SDL_FontCache.h"

#include <Eigen/Dense>

using namespace std;

class LEntity
{    
    public:
        LEntity() 
        {
            mPosition = { 0, 0 };
            mPreviousPosition = { 0, 0 };
            mAngle = 0;
            mPreviousAngle = 0;
            mScale = { 0 , 0 };
            mPreviousScale = { 0 , 0 };
            mParent = NULL;
            mTexture = NULL;
            mBounds = NULL;
            mName = "";

            mat_t = LTransform::identity();
            mat_r = LTransform::identity();
            mat_s = LTransform::identity();

            mSelectedState = false;
        };

        ~LEntity() {};        
                
        LEntity* mParent; // during deserialization, store parent id
        vector<LEntity*> mChildren;  // during deserialization, use parent reference and add self

        LTexture* mTexture;
        LBox* mBounds;

        Eigen::MatrixXd mat_t;
        Eigen::MatrixXd mat_r;
        Eigen::MatrixXd mat_s;

        // SERIALIZABLE INFO
        string mid;
        string mParentId;
        string mTextureId;
        string mName;
        SDL_Point mPosition;
        double mAngle;
        SDL_Point mScale;
        SDL_RendererFlip mFlip;

        // debug states
        bool mSelectedState;
        static string mSelectedEntity;
        static string mDeleteEntity;        

        void translateTo(int x, int y);
        void scaleTo(int x, int y);
        void rotateTo(int degree);

        void initialise(string id, string parentId, string textureId, map<string, LEntity*> &entityMap, map<string, LTexture*> &textureMap, int x=0, int y=0, int angle=0, int scale_x=1, int scale_y=1, int flip=0);
        void link(LEntity* child);
        void unlink(LEntity* child);
        LEntity* findChild(string id);
        LEntity* findParent(string id);        

        string serialize_entity();
        string serialize_scene(string scene);        

        void fixedUpdate();
        void update();        
        void render(SDL_Renderer* renderer, bool update_animation);
        
        void free(map<string, LEntity*> &entities);        
        
        void debug_display_box(FC_Font* font, SDL_Renderer* renderer, bool displayChild);

        void debug_update();
        void debug_imgui_update();
        int debug_imgui_scene_update(FC_Font* font, SDL_Renderer* renderer, map<string, LEntity*> entities, int layerIdx);
        void debug_render(FC_Font* font, SDL_Renderer* renderer);
    private:
        SDL_Point mPreviousPosition;
        SDL_Point mPreviousScale;
        int mPreviousAngle;
};
#endif
