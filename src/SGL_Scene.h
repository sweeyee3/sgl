#ifndef SGL_SCENE_H
#define SGL_SCENE_H

#include <SDL2/SDL.h>
#include <string>
#include <map>
#include <unordered_map>

#include "SGL_Texture.h"
#include "SGL_Entity.h"
#include "SGL_Timer.h"
#include "SGL_Utility.h"

using namespace std;

class LScene
{
    public:
        LScene() 
        {           
            currentLayerIndex = 0;
            currentEntityIndex = 0;
            selectedIconId = "";                      
        }
        ~LScene()
        {
            // destroy all entity instances
            for( auto const& [key, val] : entities )
            {
                delete val;
                entities[key] = NULL;                                
            }
            entities.clear();            
        }

        map<string, LEntity*> entities;
        string rootId;                         
            
        int currentLayerIndex;
        int currentEntityIndex;        
        string selectedIconId;                        

        void createLayer();
        void removeLayer(int index);        
        
        LEntity* instantiate(string textureId);
        void destroy(string key);        

        string serialize();
        void deserialize_scene(string line);
        void deserialize_entity(string line);

        void update();        
        void fixedUpdate();
        void render(SDL_Renderer* renderer);

        void free();

        void debug_update();
        void debug_imgui_update(FC_Font* font, SDL_Renderer* renderer);
        void debug_render(FC_Font* font, SDL_Renderer* renderer);        
};
#endif