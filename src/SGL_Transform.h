#ifndef SGL_TRANSFORM_H
#define SGL_TRANSFORM_H

#include <math.h>
#include <SDL2/SDL.h>
#include <Eigen/Dense>

#define PI 3.14159265
#define DEG2RAD PI/180
#define RAD2DEG 180/PI

namespace LTransform
{
    Eigen::MatrixXd pointToMatrix( int x, int y );

    Eigen::MatrixXd degreesToMatrix ( int degree );
    
    Eigen::MatrixXd scaleToMatrix ( int x_scale, int y_scale );

    Eigen::MatrixXd identity();

    Eigen::MatrixXd zero ();

    SDL_Point vectorToPoint( Eigen::VectorXd v);

    Eigen::VectorXd TRS ( Eigen::MatrixXd m_t, Eigen::MatrixXd m_r, Eigen::MatrixXd m_s, Eigen::VectorXd v );        
};
#endif