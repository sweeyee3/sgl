#include "SGL_Loader.h"
LResources* LResources::m_instance = NULL;

using namespace std;

bool LResources::loadTexture(SDL_Renderer* sdl_renderer, string image_path)
{
    //Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load( image_path.c_str() );
	if( loadedSurface == NULL )
	{
		printf( "Unable to load image %s! SDL_image Error: %s\n", image_path.c_str(), IMG_GetError() );
        return false;
	}
	else		
	{
        SDL_Texture* newTexture = NULL;

		//Color key image
		SDL_SetColorKey( loadedSurface, SDL_TRUE, SDL_MapRGB( loadedSurface->format, 0, 0xFF, 0xFF ) );
		//Create texture from surface pixels        
		newTexture = SDL_CreateTextureFromSurface( sdl_renderer, loadedSurface );
		if( newTexture == NULL )
		{
			printf( "Unable to create texture from %s! SDL Error: %s\n", image_path.c_str(), SDL_GetError() );
            return false;
		}		

		//Get rid of old loaded surface
		SDL_FreeSurface( loadedSurface );

		// insert to load texture map
        textureMap[image_path] = newTexture;
	}
    return true;
}

bool LResources::loadTextureFile(string dir_path, SDL_Renderer* renderer)
{
	bool success = true;
	int i = 0;

	// read from img folder
	for (const auto& dirEntry : filesystem::recursive_directory_iterator(dir_path))
	{		
		// check if .png extension, if so load texture
		if (dirEntry.path().extension() == ".png")
		{
			// load texture into gLoadtexture
			if ( !loadTexture(renderer, dirEntry.path()) )
			{
				printf( "Failed to load default image: %s\n", dirEntry.path().c_str() );
				success = false;
			}
			else
			{
				printf("texture loaded success: %s\n", dirEntry.path().c_str());
			}			
		}
		i++;
	}     
		

	return success;
}

bool LResources::loadMetaTextureFile( string meta_file_path, SDL_Renderer* renderer)
{
	std::fstream io;
	string line;	
	// read from meta file
	io.open(meta_file_path);
	if (io.is_open())
	{
		while (getline(io, line))
		{
			vector<string> splitStrings = LUtility::splitBy(line, ',');
			
			textureWrapperMap[splitStrings[0]] = new LTexture();
			textureWrapperMap[splitStrings[0]]->deserialize(splitStrings, textureMap[splitStrings[0]], renderer);					
		}
	}

	io.close();	

	return false;
}

bool LResources::loadFontFile(string dir_path, SDL_Renderer* renderer)
{
	bool success = true;

	for (const auto& dirEntry : filesystem::recursive_directory_iterator(dir_path))
	{		
		// check if .ttf extension, if so load texture
		if (dirEntry.path().extension() == ".ttf")
		{
			if (!loadFonts(dirEntry.path(), renderer) )
			{
				printf( "Failed to load font: %s\n", dirEntry.path().c_str() );
				success = false;
			}
			else
			{
				printf("load font success: %s\n", dirEntry.path().c_str());
			}
			
		}

	}
}

bool LResources::loadFonts(string dir_path, SDL_Renderer* renderer)
{
	fontMap[dir_path] = FC_CreateFont();
	FC_LoadFont(fontMap[dir_path], renderer, dir_path.c_str(), 30, FC_MakeColor(255, 255, 255, 255), TTF_STYLE_NORMAL);
	return true;
}

bool LResources::saveScene(string data, string dir_path)
{	
	ofstream myfile;
	myfile.open (dir_path + "/" + "scene1.scene");
	myfile << data;
	myfile.close();
	
	return false;
}

bool LResources::loadScene(string dir_path, LScene* scene)
{
	std::fstream io;
	string line;
	string deserializationState;
	bool isLoaded = false;
	io.open(dir_path);
	if (io.is_open())
	{
		while (getline(io, line))
		{			
			if (line == "ENTITIES" || line == "SCENE")
			{			
				deserializationState = line;
				printf("swapped to: %s\n", deserializationState.c_str());
				continue;
			}

			if (deserializationState == "ENTITIES")
			{
				scene->deserialize_entity(line);
			}
			else if (deserializationState == "SCENE")
			{
				scene->deserialize_scene(line);
			}														
		}
		isLoaded = true;		
	}
	io.close();		

	return isLoaded;
}

void LResources::update()
{
}

void LResources::render(SDL_Renderer* renderer)
{
}

// DEBUG
void LResources::debug_imgui_update()
{
	switch (LInputHandler::instance()->mCurrentDebugMode)
    {
		case NORMAL:
		{
            break;
		}
        case TEXTURE:
		{
			ImGui::Begin("Textures");		

			int remaining_width = 4; // TODO: calculate column count based on window width			
			int i = 0;
			is_new_selected_texture = false;		

			for (auto const& [key, val]: textureWrapperMap)
			{				
				ImGui::PushID(i);
				int frame_padding = -1;                             // -1 == uses default padding (style.FramePadding)
				ImVec2 size = ImVec2(val->mClipWidth, val->mClipHeight);                     // Size of the image we want to make visible
				ImVec2 uv0 = ImVec2(0.0f, 0.0f);                        // UV coordinates for lower-left
				ImVec2 uv1 = ImVec2((float)val->mClipWidth / (float)val->mSpritesheetWidth, (float)val->mClipHeight / (float)val->mSpritesheetHeight);
				ImVec4 bg_col = ImVec4(0.0f, 0.0f, 0.0f, 1.0f);         // Black background
				if (debug_selected_texture_id == key)
				{
					bg_col = ImVec4(0.0f, 0.0f, 0.0f, 0.75f);         // highlighted bg	
				}			
				ImVec4 tint_col = ImVec4(1.0f, 1.0f, 1.0f, 1.0f);       // No tint
				if (ImGui::ImageButton(val->mTexture, size, uv0, uv1, frame_padding, bg_col, tint_col))
				{												
					debug_selected_texture_id = key;
					is_new_selected_texture = true;												
				}			
				ImGui::PopID();		

				if (i + 1 < textureWrapperMap.size() && (i+1)%remaining_width == 0)
				{
					ImGui::NewLine();
					remaining_width = 4;				
				}
				else
				{
					ImGui::SameLine();
				}
				i++;		
			}		
			ImGui::End();	
			break;
		}		
	}	
}

void LResources::debug_update()
{
	switch (LInputHandler::instance()->mCurrentDebugMode)
    {
        case NORMAL:			
			debug_selected_texture_id = "";
            break;
        case TEXTURE:
            break;		
    }
}

void LResources::debug_render(FC_Font* font, SDL_Renderer* renderer)
{	
}

void LResources::free()
{
	for( auto const& [key, val] : textureWrapperMap )
	{		
		delete val;
		textureWrapperMap[key] = NULL;		
	}
	textureWrapperMap.clear();

	for( auto const& [key, val] : textureMap )
	{
		SDL_DestroyTexture(val);
	}
	textureMap.clear();

	for (auto const& [key, val] : fontMap)
	{
		FC_FreeFont(val);
	}

	fontMap.clear();
}