#include "SGL_Transform.h"

Eigen::MatrixXd LTransform::pointToMatrix(int x, int y )
{
    Eigen::MatrixXd m(3, 3);

    // construct a translation matrix
    m(0, 0) = 1;
    m(0, 1) = 0;
    m(0, 2) = x;
    m(1, 0) = 0;
    m(1, 1) = 1;
    m(1, 2) = y;
    m(2, 0) = 0;
    m(2, 1) = 0;
    m(2, 2) = 1;

    return m;
}

Eigen::MatrixXd LTransform::degreesToMatrix ( int degree )
{
    Eigen::MatrixXd m(3, 3);
    // construct a rotation matrix
    m(0, 0) = cos(degree * DEG2RAD);
    m(0, 1) = sin(degree * DEG2RAD);
    m(0, 2) = 0;
    m(1, 0) = -sin(degree * DEG2RAD);
    m(1, 1) = cos( degree * DEG2RAD);
    m(1, 2) = 0;
    m(2, 0) = 0;
    m(2, 1) = 0;
    m(2, 2) = 1;

    return m;
}
        
Eigen::MatrixXd LTransform::scaleToMatrix ( int x_scale, int y_scale )
{
    Eigen::MatrixXd m(3, 3);

    // construct a translation matrix
    m(0, 0) = x_scale;
    m(0, 1) = 0;
    m(0, 2) = 0;
    m(1, 0) = 0;
    m(1, 1) = y_scale;
    m(1, 2) = 0;
    m(2, 0) = 0;
    m(2, 1) = 0;
    m(2, 2) = 1;

    return m;
}

Eigen::MatrixXd LTransform::identity ()
{
    Eigen::MatrixXd m(3, 3);
    
    m(0, 0) = 1;
    m(0, 1) = 0;
    m(0, 2) = 0;
    m(1, 0) = 0;
    m(1, 1) = 1;
    m(1, 2) = 0;
    m(2, 0) = 0;
    m(2, 1) = 0;
    m(2, 2) = 1;

    return m;
}

Eigen::MatrixXd LTransform::zero ()
{
    Eigen::MatrixXd m(3, 3);
    
    m(0, 0) = 0;
    m(0, 1) = 0;
    m(0, 2) = 0;
    m(1, 0) = 0;
    m(1, 1) = 0;
    m(1, 2) = 0;
    m(2, 0) = 0;
    m(2, 1) = 0;
    m(2, 2) = 0;

    return m;
}

SDL_Point LTransform::vectorToPoint( Eigen::VectorXd v )
{
    // TODO assert if column_size = 1, row_size = 3
    return { (int) v(0), (int) v(1) };
}  

Eigen::VectorXd LTransform::TRS ( Eigen::MatrixXd m_t, Eigen::MatrixXd m_r, Eigen::MatrixXd m_s, Eigen::VectorXd v )
{
    // TODO: assert if columns / rows match
    return m_t * m_r * m_s * v;
}