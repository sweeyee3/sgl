#include "SGL_Utility.h"

vector<string> LUtility::splitBy(string line, char delimiter)
{
    // split text by comma, save to map
    string token;
    stringstream ss(line);
    vector<string> splitStrings;
    while(getline(ss, token, delimiter))
    {
        splitStrings.push_back(token);
    }
    return splitStrings;
}

