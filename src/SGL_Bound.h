#ifndef SGL_BOUND_H
#define SGL_BOUND_H

#include <SDL2/SDL.h>
#include <string>
#include "SDL_FontCache.h"
using namespace std;

class LBox
{
    public:
        // constructor
        LBox(int x, int y, int width, int height) 
        {
            mPosition = {x,y};
            mWidth = width;
            mHeight = height;
            selected = false;
        };                

        // destructor
        ~LBox() 
        {            
        };
        
        string mname;
		SDL_Point mPosition;        
        int mWidth;
		int mHeight;
        bool selected;            

        bool isInBounds(int x, int y);        

        // serialize / deserialize
        string serialize();
        void deserialize(vector<string> metaInfo);
        
        void update();        
        void fixedUpdate();
        void render(SDL_Renderer* renderer);
        
        void free();

        void debug_update();
        void debug_imgui_update();
        void debug_render(FC_Font* font, SDL_Renderer* renderer);                                 
};
#endif