#ifndef SGL_CONSTANTS_H
#define SGL_CONSTANTS_H

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const int MAX_IMAGES = 5;
const int MAX_OBJECTS = 5;
const int FONT_IMAGES = 1;
const int UPDATE_FRAMERATE = 5;

enum DebugMode
{
    NORMAL,
    TEXTURE,    
};
#endif