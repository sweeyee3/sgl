#include "SGL_InputHandler.h"
using namespace std;
LInputHandler* LInputHandler::m_instance = NULL;

void LInputHandler::pollEvent( SDL_Event* e, bool* quit )
{		
    if (e->type == SDL_QUIT)
	{		
		*quit=true;
        return;
	}	
		
	if (e->window.event == SDL_WINDOWEVENT_SIZE_CHANGED)
	{		
		// debug
		ImGui::GetIO().DisplaySize.x = static_cast<float>(e->window.data1); // register io to imgui
		ImGui::GetIO().DisplaySize.y = static_cast<float>(e->window.data2); // register io to imgui
	}

	ImGui::GetIO().DeltaTime = 1.0f / 60.0f; // register io to imgui
	
	// handle key press
    if (e->type == SDL_KEYDOWN)
	{
		switch (e->key.keysym.sym)
		{
			case SDLK_1:
			debug_display_keycode = SDLK_1;
			debug_pressed_keycode = SDLK_1;			
			break;

			case SDLK_2:
			debug_display_keycode = SDLK_2;
			debug_pressed_keycode = SDLK_2;
			break;

			case SDLK_UP:
			break;

			case SDLK_DOWN:
			break;

			case SDLK_LEFT:			
			break;

			case SDLK_RIGHT:			
			break;

			case SDLK_z:			
			break;

			case SDLK_x:			
			break;						
			
			case SDLK_SPACE:			
			break;

			case SDLK_BACKSPACE:
			debug_pressed_keycode = SDLK_BACKSPACE;			
			break;

			case SDLK_ESCAPE:
			debug_pressed_keycode = SDLK_ESCAPE;
			break;					
		}
	}	

	// handle mouse
    if( e->type == SDL_MOUSEMOTION || e->type == SDL_MOUSEBUTTONDOWN || e->type == SDL_MOUSEBUTTONUP || SDL_MOUSEWHEEL )
    {
		//reset mouse position
		prev_mouse_x = curr_mouse_x;
		prev_mouse_y = curr_mouse_y;        
		const int buttons = SDL_GetMouseState( &curr_mouse_x, &curr_mouse_y );

		ImGui::GetIO().MousePos = ImVec2(static_cast<float>(curr_mouse_x), static_cast<float>(curr_mouse_y)); // register io to imgui
		ImGui::GetIO().MouseDown[0] = buttons & SDL_BUTTON(SDL_BUTTON_LEFT); // register io to imgui
		ImGui::GetIO().MouseDown[1] = buttons & SDL_BUTTON(SDL_BUTTON_RIGHT); // register io to imgui

        switch( e->type )
        {
            case SDL_MOUSEMOTION:
			mouse_delta_x = curr_mouse_x - prev_mouse_x;
			mouse_delta_y = curr_mouse_y - prev_mouse_y;
			// printf("(x: %i, y: %i)\n", mouse_delta_x, mouse_delta_y);									
            break;
        
            case SDL_MOUSEBUTTONDOWN:
			mouse_left_pressed = true;
			mouse_down = true;            
            break;
            
            case SDL_MOUSEBUTTONUP:
			mouse_released = true;
			mouse_down = false;            
            break;

			case SDL_MOUSEWHEEL:
			wheel = e->wheel.y;
			break;
        }
    }	
}

void LInputHandler::update(SDL_Event* eventInput, bool* quit)
{		
	// reset variables
	{
		mouse_left_pressed = false;
		mouse_released = false;                  	

		// reset delta value
		mouse_delta_x = 0;
		mouse_delta_y = 0;

		debug_pressed_keycode = 0;	
	}

	while ( SDL_PollEvent(eventInput) != 0 )
	{																																																						
		// process input here					
		LInputHandler::instance()->pollEvent(eventInput, quit);										
	}
}

void LInputHandler::render(SDL_Renderer* renderer)
{
}

void LInputHandler::debug_update()
{	
}

void LInputHandler::debug_imgui_update()
{
	// handle debug io
	ImGui::GetIO().DeltaTime = 1.0f / 60.0f;
	ImGui::GetIO().MousePos = ImVec2(static_cast<float>(curr_mouse_x), static_cast<float>(curr_mouse_y));
	ImGui::GetIO().MouseDown[0] = mouse_down & SDL_BUTTON(SDL_BUTTON_LEFT);
	ImGui::GetIO().MouseDown[1] = mouse_down & SDL_BUTTON(SDL_BUTTON_RIGHT);
	ImGui::GetIO().MouseWheel = static_cast<float>(wheel);	
}

void LInputHandler::debug_render(FC_Font* font, SDL_Renderer* renderer)
{	
	string debug_info = "";
	FC_Effect effects = { FC_ALIGN_LEFT, { 0.5, 0.5 }, FC_MakeColor(255, 255, 255, 255) }; 
			
	// display current mouse position
	debug_info += "mouse position: (" + to_string(curr_mouse_x) + "," + to_string(curr_mouse_y) + ")\n";

	// display imgui			
	// debug_info += "imgui mouse position: (" + to_string(ImGui::GetIO().MousePos.x) + "," + to_string(ImGui::GetIO().MousePos.y) + ")\n";		

	// display mouse state
	debug_info += "mouse pressed state: " + to_string(mouse_down) + "\n";	

	FC_DrawEffect(font, renderer, 0, 0, effects, debug_info.c_str());			
}