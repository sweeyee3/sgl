#include "SGL_Scene.h"
#include "SGL_Loader.h"
#include "SGL_InputHandler.h"

void LScene::render(SDL_Renderer* renderer)
{
    for (int i=0; i<entities[rootId]->mChildren.size();i++)
    {
        entities[rootId]->mChildren[i]->render( renderer, true );                
    }    
}

void LScene::debug_render(FC_Font* font, SDL_Renderer* renderer)
{
    // render the selected entity
    switch (LInputHandler::instance()->mCurrentDebugMode)
    {
        case NORMAL:

            break;
        case TEXTURE:
        {
            // if spawning in entity            
            if (!ImGui::IsWindowHovered(ImGuiHoveredFlags_AnyWindow) && selectedIconId != "")
            {
                if (entities[selectedIconId]->mBounds != NULL)
                {
                    entities[selectedIconId]->mBounds->render(renderer);
                    // display centre
                    SDL_Rect fillRect = { entities[selectedIconId]->mPosition.x, entities[selectedIconId]->mPosition.y, 4, 4 };
                    SDL_SetRenderDrawColor( renderer, 0xFF, 0x00, 0x00, 0xFF );        
                    SDL_RenderFillRect( renderer, &fillRect );
                }
            }                
                        
            // when hovering over entity without any selection, render
            if (LResources::instance()->debug_selected_texture_id == "")
            {
                for( auto const& [key, val] : entities )
                {
                    if (val->mBounds != NULL && val->mBounds->isInBounds(LInputHandler::instance()->curr_mouse_x, LInputHandler::instance()->curr_mouse_y))
                    {
                        val->debug_render(font, renderer);
                    }
                }
            }
            break;        
        }
    }        
}

string LScene::serialize()
{
    string data = "";

    data += "ENTITIES\n";
    // save entity info
    for( auto const& [key, val] : entities )
    {        
        data += entities[key]->serialize_entity();
        data += "\n";                                
    }

    data += "SCENE\n";
    data += "root:" + rootId + "\n";

    // save scene info
    for (int i=0; i<entities[rootId]->mChildren.size();i++)
    {
        // save entities
        data = entities[rootId]->serialize_scene(data);                
    }

    return data;
}

void LScene::deserialize_scene(string line)
{       
    // deserialize scene information: first line is rootId, then populate entity with informtion
    vector<string> colonSplit = LUtility::splitBy(line, ':');
    if (colonSplit[0] == "root")
    {
        // set root id
        rootId = colonSplit[1];        
    }
    else
    {
        if (colonSplit.size() > 1)
        {
            vector<string> commaSplit = LUtility::splitBy(colonSplit[1], ',');                        
            for (int i=0; i<commaSplit.size(); i++)
            {
                // set parent's child
                // printf ("before parent: %lu\n", entities.size());                
                LEntity* parent = entities[colonSplit[0]];
                // printf ("after parent: %s\n", parent->mid.c_str());
                entities[colonSplit[0]]->mChildren.push_back(entities[commaSplit[i]]);

                // set child's parent
                entities[commaSplit[i]]->mParentId = colonSplit[0];
                entities[commaSplit[i]]->mParent = entities[colonSplit[0]];                                
            }
        }        
    }    

    printf ("deserialized: %s\n", line.c_str());    
}

void LScene::deserialize_entity(string line)
{
    // split the line
    vector<string> metaInfo = LUtility::splitBy(line, ',');

    string id = metaInfo[0];
    SDL_Point pos = { stoi(metaInfo[1]), stoi(metaInfo[2]) };
    int angle = stoi(metaInfo[3]);
    SDL_Point scale = { stoi(metaInfo[4]) , stoi(metaInfo[5]) };    
    int flip = stoi(metaInfo[6]);
    string parentId = metaInfo[7];
    string textureId = metaInfo[8];

    // create entity
    LEntity* entity = new LEntity();    

    entity->initialise(id, parentId, textureId, entities, LResources::instance()->textureWrapperMap, pos.x, pos.y, angle, scale.x, scale.y, flip);
    if (entities.find(entity->mid) == entities.end())
    {
        entities[entity->mid] = entity;                                    
    }
    printf ("deserialized: %s\n", entities[id]->mid.c_str());
}

// Layer
void LScene::createLayer()
{    
    LEntity* layer = instantiate("");
    entities[rootId]->link(layer);        
    // entities[rootId]->mChildren.push_back(layer);        

    layer->mName = "layer_" + to_string(entities[rootId]->mChildren.size());    
}

void LScene::removeLayer(int index)
{
    LEntity* layer = entities[rootId]->mChildren[index];                        
    destroy(layer->mid);
    if (currentLayerIndex >= entities[rootId]->mChildren.size()) // reset layer index to 0 if it exceeds the bounds
    {
        currentLayerIndex = entities[rootId]->mChildren.size() - 1;
    }        
}

// Entity
LEntity* LScene::instantiate(string textureId)
{
    LEntity* entity = new LEntity();

    string id = to_string(random());
    while (entities.find(id) != entities.end()) // if exist in map
    {            
        id = to_string(random()); // TODO: resolve hash conflict in a better way
    }

    // initialise entity
    entity->initialise(id, "", textureId, entities, LResources::instance()->textureWrapperMap);

    // add to entities map       
    if (entities.find(entity->mid) == entities.end())
    {
        entities[entity->mid] = entity;                                    
    }        

    return entity;                       
}

void LScene::destroy(string key)
{
    printf("before %lu\n", entities.size());
    LEntity* dEntity = entities[key];

    // remove from entities map
    if (key != "")
    {        
        // unlink self from parent    
        if (dEntity->mParentId != "")
        {                        
            LEntity* e = entities[dEntity->mParentId];            
            e->unlink(dEntity);
        }
        dEntity->free(entities);        
        // if (entities[key] != NULL)                                          
        entities.erase(key); // must ensure that key is NULL
        
        delete dEntity;        
    }   

    printf("-- after %lu --\n", entities.size()); 
}

void LScene::update()
{
    for( auto const& [key, val] : entities )
	{		
		val->update();
	}
}

void LScene::debug_update()
{
    // update entities
    for( auto const& [key, val] : entities )
	{		
		val->debug_update();
	}   

    // swtich mode
    switch (LInputHandler::instance()->mCurrentDebugMode)
    {
        case NORMAL:
            switch (LInputHandler::instance()->debug_pressed_keycode)
            {
                case SDLK_1:
                    LInputHandler::instance()->mCurrentDebugMode = TEXTURE;                    
                    break;                
                case SDLK_ESCAPE:                    
                    break;            
            } 
            break;
        case TEXTURE:            
            // switch key presses
            switch (LInputHandler::instance()->debug_pressed_keycode)
            {
                case SDLK_1:
                {
                    LInputHandler::instance()->mCurrentDebugMode = NORMAL;
                    LResources::instance()->debug_selected_texture_id = ""; // when press escape, set to nothing                                                

                    if (selectedIconId != "")
                    {
                        destroy(selectedIconId);
                        selectedIconId = "";
                    }                    
                    break;
                }
                case SDLK_2:                    
                    break;
                case SDLK_ESCAPE:
                {
                    LResources::instance()->debug_selected_texture_id = ""; // when press escape, set to nothing                    
                    if (selectedIconId != "")
                    {
                        destroy(selectedIconId);
                        selectedIconId = "";
                    }                     
                    break;
                }
                case SDLK_BACKSPACE:
                {
                    if (LResources::instance()->debug_selected_texture_id == "")
                    {                           
                        string dKey = "";
                        
                        for( auto const& [key, val] : entities )
                        {
                            if (val->mBounds != NULL && val->mBounds->isInBounds(LInputHandler::instance()->curr_mouse_x, LInputHandler::instance()->curr_mouse_y))
                            {                                
                                dKey = key;                                
                                break;
                            }            
                        }
                        
                        if (dKey != "")
                        {                            
                            destroy(dKey); // remove from map
                        }
                    }
                    break;
                }
            }
            
            if (selectedIconId != "")
                entities[selectedIconId]->translateTo(LInputHandler::instance()->curr_mouse_x, LInputHandler::instance()->curr_mouse_y);
            break;        
    }
}

void LScene::debug_imgui_update(FC_Font* font, SDL_Renderer* renderer)
{
    // MODE
    switch (LInputHandler::instance()->mCurrentDebugMode)
    {
        case NORMAL:
        {
            break;
        }
        case TEXTURE:
        {
            // UDPATE LOADER ICON INPUTS
            if (!ImGui::IsWindowHovered(ImGuiHoveredFlags_AnyWindow | ImGuiHoveredFlags_AllowWhenBlockedByActiveItem))
            {                
                // if texture is currently selected                
                if (selectedIconId == "" && LResources::instance()->debug_selected_texture_id != "")
                {                    
                    // set a box that follows the cursor by instantiating entity first
                    string textureId = LResources::instance()->debug_selected_texture_id;
                    LEntity* icon = instantiate(textureId);
                    icon->translateTo( LInputHandler::instance()->curr_mouse_x , LInputHandler::instance()->curr_mouse_y  );
                    selectedIconId = icon->mid;                    
                }

                // check if mouse pressed and hovering outside of imgui
                if (LInputHandler::instance()->mouse_left_pressed && LResources::instance()->debug_selected_texture_id != "" ) 
                {                
                    LEntity* e = instantiate(LResources::instance()->debug_selected_texture_id); // add entity to map
                    entities[rootId]->mChildren[currentLayerIndex]->link(e); // add entity to scene graph                                                                                

                    e->translateTo( LInputHandler::instance()->curr_mouse_x , LInputHandler::instance()->curr_mouse_y  ); // translate to mouse position                    
                }
            }
            else
            {
                if (selectedIconId != "" && LResources::instance()->is_new_selected_texture)
                {                    
                    destroy(selectedIconId);
                    selectedIconId = "";                    
                }
            }                

            // UPDATE LAYERS                
            ImGui::Begin("Layers", NULL, ImGuiWindowFlags_MenuBar);
            
            // scrolling

            // menu bar
            if (ImGui::BeginMenuBar())
            {
                if (ImGui::BeginMenu("Menu"))
                {
                    bool isCreateLayer = false;                    

                    ImGui::MenuItem("Create Layer", NULL, &isCreateLayer);                    
                    ImGui::EndMenu();

                    if (isCreateLayer) createLayer();                    
                }

                ImGui::EndMenuBar();
            }            
            
            // shift layer up function
            auto shifftLayerUp = [](LEntity* root, int i, int payload_i)
            {                        
                int idx = i;
                LEntity* curr;
                LEntity* pay = root->mChildren[payload_i];

                while ( idx >= payload_i )
                {
                    curr = root->mChildren[idx];
                    root->mChildren[idx] = pay;                                             
                    pay = curr;                                                
                    idx --;                                               
                }                
            };

            // shift layer down function
            auto shiftLayerDown = [](LEntity* root, int i, int payload_i)
            {
                int idx = i;
                LEntity* curr;
                LEntity* pay = root->mChildren[payload_i];

                while ( idx <= payload_i )
                {
                    curr = root->mChildren[idx];            
                    root->mChildren[idx] = pay;                                             
                    pay = curr;                                                
                    idx ++;
                }                        
            };

            int layerToDelete = -1;

            // setup layer tree            
            for (int i=0; i<entities[rootId]->mChildren.size();i++)
            {
                string layerName = "layer" + to_string(i);                
                ImGui::PushID(entities[rootId]->mChildren[i]->mid.c_str());

                // top selectable ( accept payload )
                if ( i == 0 )
                {
                    ImGui::Selectable("", false, ImGuiSelectableFlags_Disabled, ImVec2{0, 0.25}); // Pad with space to extend size horizontally
                    if (ImGui::BeginDragDropTarget())
                    {
                        const ImGuiPayload* test_payload = ImGui::GetDragDropPayload();
                        if (test_payload != NULL)
                        {
                            // IM_ASSERT(test_payload->DataSize == sizeof(int));
                            if (test_payload->DataSize == sizeof(int))
                            {
                                int payload_i = *(const int*)test_payload->Data;
                                if (payload_i != 0)
                                {
                                    if (ImGui::AcceptDragDropPayload("LayerCell"))
                                    {
                                        shiftLayerDown(entities[rootId], i, payload_i);                                    
                                    }
                                }
                            }                            
                        }
                        ImGui::EndDragDropTarget();
                    }
                }     

                int guiFlag = ImGuiTreeNodeFlags_NoTreePushOnOpen | ImGuiTreeNodeFlags_NoAutoOpenOnLog;
                if (currentLayerIndex == i)
                {                    
                    guiFlag |= ImGuiTreeNodeFlags_Selected;
                }
                if (entities[rootId]->mChildren[i]->mChildren.size() == 0)
                {
                    guiFlag |= ImGuiTreeNodeFlags_Bullet;
                }        

                bool treeNode = ImGui::TreeNodeEx(layerName.c_str(),  guiFlag);

                bool popup = ImGui::BeginPopupContextItem("layer", ImGuiPopupFlags_MouseButtonRight);	
                if (popup)
                {
                    if (ImGui::Selectable("delete")) layerToDelete = i;                    				
                    ImGui::EndPopup();
                }                                            

                if (entities[rootId]->mChildren[i]->mSelectedState != treeNode)
                {
                    currentLayerIndex = i;
                    entities[rootId]->mChildren[i]->mSelectedState = treeNode; // set the layer's selected state
                }                                

                if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_None))
                {
                    // Set payload to carry the index of our item (could be anything)
                    ImGui::SetDragDropPayload("LayerCell", &i, sizeof(int));

                    // Display preview (could be anything, e.g. when dragging an image we could decide to display
                    // the filename and a small preview of the image, etc.)
                    ImGui::Text("Moving %s", layerName.c_str());
                    ImGui::EndDragDropSource();
                }

                // set this entity as child of target parent
                if (ImGui::BeginDragDropTarget())
                {                    
                    const ImGuiPayload* test_payload = ImGui::GetDragDropPayload();
                    if (test_payload != NULL && test_payload->IsDataType("EntityCell"))
                    {
                        IM_ASSERT(test_payload->DataSize == sizeof(tuple<int, string, int, string>));
                        tuple<int, string, int, string> payload_j = *(const tuple<int, string, int, string>*)test_payload->Data;
                        if (ImGui::AcceptDragDropPayload("EntityCell"))
                        {                            
                            tuple<int, string, int, string> child_payload_mid = *(const tuple<int, string, int, string>*)test_payload->Data;                                                            
                            
                            // id of parent and child to be removed
                            string removeParentId = entities[get<1>(child_payload_mid)]->mParentId;                            

                            // unlink from old parent
                            if (removeParentId != "")
                                entities[removeParentId]->unlink(entities[get<1>(child_payload_mid)]);

                            // link to new parent
                            entities[rootId]->mChildren[i]->link(entities[get<1>(child_payload_mid)]);                                                        
                        }
                    }
                    
                    ImGui::EndDragDropTarget();
                }

                if (treeNode)
                {
                    ImGui::Indent();                        
                    entities[rootId]->mChildren[i]->debug_imgui_scene_update(font, renderer, entities, i);
                    ImGui::Unindent();                                        
                }                                                

                // bottom selectable
                ImGui::Selectable("", false, ImGuiSelectableFlags_Disabled, ImVec2{0, 0.25}); // Pad with space to extend size horizontally
                if (ImGui::BeginDragDropTarget())
                {
                    const ImGuiPayload* test_payload = ImGui::GetDragDropPayload();
                    if (test_payload != NULL && test_payload->IsDataType("LayerCell"))
                    {
                        // IM_ASSERT(test_payload->DataSize == sizeof(int));
                        if (test_payload->DataSize == sizeof(int))
                        {
                            int payload_i = *(const int*)test_payload->Data;
                            if (payload_i > i) // if coming from bottom, shift everything down.
                            {                                    
                                if (ImGui::AcceptDragDropPayload("LayerCell"))
                                {
                                    shiftLayerDown(entities[rootId], i + 1, payload_i);                                
                                }
                            }
                            else if (payload_i < i) // if comming from the top, shift everything up
                            {
                                if (ImGui::AcceptDragDropPayload("LayerCell"))
                                {
                                    shifftLayerUp(entities[rootId], i, payload_i);                                
                                }
                            }
                        }
                    }
                    ImGui::EndDragDropTarget();
                }                

                ImGui::PopID();
            }

            // right click anywhere for context menu
            bool popup = ImGui::BeginPopupContextWindow("layer context", ImGuiPopupFlags_MouseButtonRight | ImGuiPopupFlags_NoOpenOverExistingPopup);	
            if (popup)
            {
                if (ImGui::Selectable("create layer")) 
                {
                    createLayer();
                }
                ImGui::EndPopup();
            }            

            if (layerToDelete >= 0)
            {
                removeLayer(layerToDelete);
                layerToDelete = -1;
                printf("here\n");
            }
            if (LEntity::mDeleteEntity != "")
            {
                printf("here: %s\n", LEntity::mDeleteEntity.c_str());
                destroy(LEntity::mDeleteEntity);
                LEntity::mDeleteEntity = "";
            }

            ImGui::End();
            break;
        }        
    }
}