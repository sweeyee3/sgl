#include "SGL_Bound.h"

bool LBox::isInBounds(int x, int y)
{
	// if ( x > mPosition.x - (mWidth / 2)  
	// 	&& x < mPosition.x + (mWidth / 2) 
	// 	&& y > mPosition.y - (mHeight / 2)
	// 	&& y < mPosition.y + (mHeight / 2))
	// 	{
	// 		printf("%s position: ( %i, %i )\n", mname.c_str(), mPosition.x - (mWidth / 2), mPosition.x + (mWidth / 2));
	// 		// printf("%s: inside box\n", mname.c_str());
	// 		return true;
	// 	}

	if ( x > mPosition.x
		&& x < mPosition.x + mWidth 
		&& y > mPosition.y 
		&& y < mPosition.y + mHeight)
		{
			// printf("%s position: ( %i, %i )\n", mname.c_str(), mPosition.x - (mWidth / 2), mPosition.x + (mWidth / 2));
			// printf("%s: inside box\n", mname.c_str());
			return true;
		}
	return false;
}

void LBox::render(SDL_Renderer* renderer)
{	
	SDL_Rect outlineRect = { mPosition.x, mPosition.y, mWidth, mHeight };
	SDL_SetRenderDrawColor( renderer, 0x00, 0xFF, 0x00, 0xFF );        
	SDL_RenderDrawRect( renderer, &outlineRect );
}

void LBox::free()
{
}