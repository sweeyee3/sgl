#ifndef SGL_TIMER_H
#define SGL_TIMER_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <string>
#include <sstream>
#include "SDL_FontCache.h"
using namespace std;

class LTimer
{
    public:
        static LTimer *instance()
        {
            if (!m_instance)
            {
                m_instance = new LTimer();
            }
            return m_instance;
        }        
        
        // stores total number of frames        
        uint mTotalFrames;

        Uint32 getTicks();
        
        void start();
        void stop();
        void pause();
        void unpause();

        bool isStarted();
        bool isPaused();

        void update();
        void fixedUpdate();
        void render(SDL_Renderer* renderer);

        void free();

        void debug_update();
        void debug_imgui_update();
        void debug_render(FC_Font* font, SDL_Renderer* renderer);                

    private:
        LTimer() 
        {
            //Initialize the variables
            mStartTicks = 0;
            mPausedTicks = 0;

            mPaused = false;
            mStarted = false;
        };
        ~LTimer() {};

        static LTimer *m_instance;
                
        //The clock time when the timer started
        Uint32 mStartTicks;

        //The ticks stored when the timer was paused
        Uint32 mPausedTicks;        

        //The timer status
        bool mPaused;
        bool mStarted;

        stringstream debugText;                
};
#endif