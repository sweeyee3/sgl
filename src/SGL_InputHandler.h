#ifndef SGL_INPUTHANDLER_H
#define SGL_INPUTHANDLER_H

#include <SDL2/SDL.h>
#include <string>

#include "DearImGui/imgui.h"
#include "imgui_sdl/imgui_sdl.h"

#include "SGL_Constants.h"
#include "SDL_FontCache.h"

using namespace std;

class LInputHandler
{
    public:        

        static LInputHandler *instance()
        {
            if (!m_instance)
            {
                m_instance = new LInputHandler();
            }
            return m_instance;
        }

        // mouse position
        int curr_mouse_x, curr_mouse_y, prev_mouse_x, prev_mouse_y;
        int mouse_delta_x, mouse_delta_y;

        // mouse pressed    
        bool mouse_left_pressed;
        bool mouse_released;                
        bool mouse_down;

        // key presses
        SDL_Keycode debug_pressed_keycode;                        

        // debug info
        SDL_Keycode debug_display_keycode;

        // current mode
        DebugMode mCurrentDebugMode;                

        //poll input event        
        void pollEvent( SDL_Event* e, bool* quit );        

        string serialize();
        void deserialize(vector<string> metaInfo);

        void fixedUpdate();
        void update(SDL_Event* eventInput, bool* quit);
        void render(SDL_Renderer* renderer);
        
        void free();

        void debug_update();
        void debug_imgui_update();
        void debug_render(FC_Font* font, SDL_Renderer* renderer);
    
    private:
        // constructor
        LInputHandler() 
        {            
            mouse_down = false;
            mouse_left_pressed = false;
            mouse_released = false;

            debug_display_keycode = SDLK_1;
            wheel = 0;
            mCurrentDebugMode = NORMAL;
            debug_pressed_keycode = 0;
        };                

        // destructor
        ~LInputHandler() 
        {
            free();
        };    
                
        int wheel;
        static LInputHandler *m_instance;        
};
#endif