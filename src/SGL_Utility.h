#ifndef SGL_UTILITY_H
#define SGL_UTILITY_H

#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

namespace LUtility
{
    vector<string> splitBy(string line, char delimiter);    
}
#endif
