#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <string>
#include <sstream>
#include "SGL_Timer.h"
#include "SGL_Constants.h"
#include "SDL_FontCache.h"
using namespace std;
LTimer* LTimer::m_instance = NULL;

void LTimer::start()
{
    //Start the timer
    mStarted = true;

    //Unpause the timer
    mPaused = false;

    //Get the current clock time
    mStartTicks = SDL_GetTicks();
    mPausedTicks = 0;
}

void LTimer::stop()
{
    //Stop the timer
    mStarted = false;

    //Unpause the timer
    mPaused = false;

    //Clear tick variables
    mStartTicks = 0;
    mPausedTicks = 0;
}

void LTimer::pause()
{
    //If the timer is running and isn't already paused
    if( mStarted && !mPaused )
    {
        //Pause the timer
        mPaused = true;

        //Calculate the paused ticks
        mPausedTicks = SDL_GetTicks() - mStartTicks;
        mStartTicks = 0;
    }
}

void LTimer::unpause()
{
    //If the timer is running and paused
    if( mStarted && mPaused )
    {
        //Unpause the timer
        mPaused = false;

        //Reset the starting ticks
        mStartTicks = SDL_GetTicks() - mPausedTicks;

        //Reset the paused ticks
        mPausedTicks = 0;
    }
}

void LTimer::update()
{
    mTotalFrames++;
}

bool LTimer::isStarted()
{
    //Timer is running and paused or unpaused
    return mStarted;
}

bool LTimer::isPaused()
{
    //Timer is running and paused
    return mPaused && mStarted;
}

Uint32 LTimer::getTicks()
{
    //The actual timer time
    Uint32 time = 0;

    //If the timer is running
    if( mStarted )
    {
        //If the timer is paused
        if( mPaused )
        {
            //Return the number of ticks when the timer was paused
            time = mPausedTicks;
        }
        else
        {
            //Return the current time minus the start time
            time = SDL_GetTicks() - mStartTicks;
        }
    }

    return time;
}

void LTimer::render(SDL_Renderer* renderer)
{
}

void LTimer::debug_update()
{
}

void LTimer::debug_imgui_update()
{
}

void LTimer::debug_render(FC_Font* font, SDL_Renderer* renderer)
{
    // display time    
    debugText.str( "" );

    if (!mPaused)
    {
        debugText << "time: " << SDL_GetTicks() - mStartTicks << "\n";
    }
    else
    {
        debugText << "time: " << mPausedTicks << "\n";
    }    

    // display fps    
    float avgFPS = mTotalFrames / ( getTicks() / 1000.f );
    if( avgFPS > 2000000 )
    {
        avgFPS = 0;
    }

    debugText << "fps: " << avgFPS << "\n";

    FC_Effect effects = { FC_ALIGN_LEFT, { 0.5, 0.5 }, FC_MakeColor(255, 255, 255, 255) }; 
    FC_DrawEffect(font, renderer, 0, 0, effects, debugText.str().c_str());    
}