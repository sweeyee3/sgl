#include "SGL_Entity.h"
#include "SGL_InputHandler.h"
string LEntity::mSelectedEntity = "";
string LEntity::mDeleteEntity = "";

void LEntity::translateTo(int x, int y)
{
    mPosition = { x, y };    
    
    // don't update if there's no change in position
    if (mPreviousPosition.x == mPosition.x && mPreviousPosition.y == mPosition.y)
    {
        return;
    }

    if (mTexture != NULL)
        mTexture->mPosition = mPosition;

    if (mBounds != NULL)
        mBounds->mPosition = mPosition;    

    // translate any children ( TODO: check efficiency )
    for ( int i = 0; i < mChildren.size(); i++)
    {
        mat_t = LTransform::pointToMatrix( mPosition.x - mPreviousPosition.x, mPosition.y - mPreviousPosition.y );

        Eigen::VectorXd v(3);
        v[0] = mChildren[i]->mPosition.x;
        v[1] = mChildren[i]->mPosition.y;
        v[2] = 1;
        Eigen::MatrixXd p_v = LTransform::TRS( mat_t, mat_r, mat_s, v);
        SDL_Point newPosition = LTransform::vectorToPoint( p_v );
        mChildren[i]->translateTo( newPosition.x, newPosition.y );    
    }
    mPreviousPosition = mPosition;    
}

void LEntity::scaleTo(int x, int y)
{
    mScale = { x, y };

    // TODO: update texture / bounds

    for ( int i = 0; i < mChildren.size(); i++)
    {
        mat_s = LTransform::scaleToMatrix( mScale.x - mPreviousScale.x, mScale.y - mPreviousScale.y );

        Eigen::VectorXd v(3);
        v[0] = mChildren[i]->mPosition.x;
        v[1] = mChildren[i]->mPosition.y;
        v[2] = 1;
        Eigen::MatrixXd p_v = LTransform::TRS( mat_t, mat_r, mat_s, v);
        SDL_Point newPosition = LTransform::vectorToPoint( p_v );
        mChildren[i]->translateTo( newPosition.x, newPosition.y );    
    }
    mPreviousScale = mScale;
}

void LEntity::rotateTo(int degree)
{
    mAngle = degree;

    // TODO: update texture / bounds

    for ( int i = 0; i < mChildren.size(); i++)
    {
        mat_s = LTransform::degreesToMatrix( mAngle - mPreviousAngle );

        Eigen::VectorXd v(3);
        v[0] = mChildren[i]->mPosition.x;
        v[1] = mChildren[i]->mPosition.y;
        v[2] = 1;
        Eigen::MatrixXd p_v = LTransform::TRS( mat_t, mat_r, mat_s, v);
        SDL_Point newPosition = LTransform::vectorToPoint( p_v );
        mChildren[i]->translateTo( newPosition.x, newPosition.y );    
    }
    mPreviousAngle = mAngle;
}

void LEntity::initialise(string id, string parentId, string textureId, map<string, LEntity*> &entityMap, map<string, LTexture*> &textureMap, int x, int y, int angle, int scale_x, int scale_y, int flip)
{    
    if (id != "")
    {        
        mid = id;        
    }

    mParentId = parentId;
    mTextureId = textureId;

    mPosition = { x, y };
    mAngle = angle;
    mScale = { scale_x , scale_y };    
    switch (flip)
    {
        case 0:
            mFlip = SDL_FLIP_NONE;            
            break;
        case 1:
            mFlip = SDL_FLIP_HORIZONTAL;            
            break;
        case 2:
            mFlip = SDL_FLIP_VERTICAL;            
            break;
        default:
            mFlip = SDL_FLIP_NONE;
            break;            
    }    

    if (entityMap.find(parentId) != entityMap.end()) // check if parent exists
    {
        mParent = entityMap[parentId]; // set parent reference
        entityMap[parentId]->mChildren.push_back(this); // set child reference                
    }
    // check if texture exists in map
    if (textureMap.find(mTextureId) != textureMap.end())
    {        
        mTexture = new LTexture(textureMap[mTextureId]); // must make a copy of LTexture        
    }    
    
    // check if texture is not null
    if (mTexture != NULL)
    {
        mBounds = new LBox( 0, 0, mTexture->mClipWidth, mTexture->mClipHeight );
        mName = mTexture->mname;
    }

    translateTo(mPosition.x , mPosition.y);    
}

void LEntity::link(LEntity* child)
{
    // TODO: check if the parent's children does not contain the parent

    // set child to new parent
    printf("before linked parent id: %s\n", child->mParentId.c_str());
    child->mParentId = mid;
    child->mParent = this;
    printf("after linked parent id: %s\n", child->mParentId.c_str());                    

    // set new parent link to child
    mChildren.push_back(child);
}

void LEntity::unlink(LEntity* child)
{    
    // set child to blank
    child->mParentId = "";
    child->mParent = NULL;

    // remove child from parent
    auto iter = find( mChildren.begin(), mChildren.end(), child );    
    if (iter != mChildren.end())
    {
        mChildren.erase(iter);        
    }
}

LEntity* LEntity::findChild(string id)
{
    // do a deep search to find if e exists
    if (mid == id)
    {
        return this;
    }
    else if (mChildren.size() == 0)
    {
        return NULL;
    }
    else
    {
        for (int i = 0; i<mChildren.size(); i++)
        {
            return mChildren[i]->findChild(id);
        } 
    }
}

LEntity* LEntity::findParent(string id)
{
    // do a search to find if parent exists
    if (mid == id)
    {        
        return mParent;
    }
    else if (mParentId == "") // if parent is the root entity or is empty
    {        
        return NULL;
    }
    else
    {        
        return mParent->findParent(id);
    }
}

/* data format:
    id, x, y, angle, scale_x, scale_y, flip, parentID, textureID
*/
string LEntity::serialize_entity()
{
    string data = "";
    data += mid + ",";
    data += to_string(mPosition.x) + "," + to_string(mPosition.y) + ",";
    data += to_string(mAngle) + ",";
    data += to_string(mScale.x) + "," + to_string(mScale.y) + ",";
    switch (mFlip)
    {
        case SDL_FLIP_NONE:
            data += "0,";            
            break;
        case SDL_FLIP_HORIZONTAL:
            data += "1,";            
            break;
        case SDL_FLIP_VERTICAL:
            data += "2,";            
            break;
        default:
            data += "0,";
            break;            
    }
    data += mParentId + ",";
    
    if (mTexture != NULL)
    {
        data += mTexture->mname;
    }
    else
    {
        data += "";
    }    

    return data;
}

string LEntity::serialize_scene(string scene)
{
    scene += mid + ":";
    for (int i = 0; i<mChildren.size(); i++)
    {
        scene += mChildren[i]->mid;
        if (i < mChildren.size() - 1) scene += ',';                
    }

    scene += "\n";

    for (int i = 0; i<mChildren.size(); i++)
    {
        scene = mChildren[i]->serialize_scene(scene);        
    }

    return scene;
}
        
void LEntity::update()
{        
}

void LEntity::fixedUpdate()
{ 
}

void LEntity::render(SDL_Renderer* renderer, bool update_animation)
{
    if (mTexture != NULL)
        mTexture->render(mAngle, mFlip);
        
    // render children  ( TO BE REMOVED WITH CUSTOM RENDER ORDER )
    for (int i=0; i<mChildren.size(); i++)
    {
        mChildren[i]->render(renderer, update_animation);
        // printf("name: %s\n", mName.c_str());
    }
}

void LEntity::free(map<string, LEntity*> &entities)
{
    // destroy assets
    if (mBounds != NULL)
    {
        delete mBounds;
        mBounds = NULL;
    }
    if (mTexture != NULL)
    {
        delete mTexture;
        mTexture = NULL;
    }

    // delete child from the last element
    int idx = mChildren.size() - 1;
    LEntity* e;
    while (idx >= 0)
    {
        e = entities[mChildren[idx]->mid];        
        mChildren.erase(mChildren.begin() + idx);
        e->free(entities);        
        delete e;
        idx--;
    }        
        
    // erase self        
    printf("free before %lu\n", entities.size());
    entities.erase(mid); // remove from entities
    printf("free after %lu\n", entities.size());                           
}

void LEntity::debug_display_box(FC_Font* font, SDL_Renderer* renderer, bool displayChild)
{
    if ( mBounds != NULL )
    {
        string debug_info = "(" + to_string(mPosition.x) + "," + to_string(mPosition.y) + ")";
        FC_Effect effects = { FC_ALIGN_CENTER, { 0.5, 0.5 }, FC_MakeColor(255, 255, 255, 255) };
        FC_DrawEffect(font, renderer, mPosition.x + mTexture->mClipWidth / 2 , mPosition.y + mTexture->mClipHeight + 8, effects, debug_info.c_str());

        mBounds->render(renderer);

        // display centre
        SDL_Rect fillRect = { mPosition.x, mPosition.y, 4, 4 };
        SDL_SetRenderDrawColor( renderer, 0xFF, 0x00, 0x00, 0xFF );        
        SDL_RenderFillRect( renderer, &fillRect );
    }
    // display position of children
    for (int i = 0; i<mChildren.size(); i++)
    {
        mChildren[i]->debug_display_box(font, renderer, displayChild);                    
    }
}

void LEntity::debug_render(FC_Font* font, SDL_Renderer* renderer)
{
    debug_display_box(font, renderer, true);
}

void LEntity::debug_update()
{
    if (mBounds != NULL && !ImGui::IsWindowHovered(ImGuiHoveredFlags_AnyWindow | ImGuiHoveredFlags_AllowWhenBlockedByActiveItem))
    {
        // set selection
        if (LInputHandler::instance()->mouse_left_pressed && !mBounds->isInBounds( LInputHandler::instance()->curr_mouse_x, LInputHandler::instance()->curr_mouse_y ))
            mBounds->selected = false;

        if (LInputHandler::instance()->mouse_left_pressed && mBounds->isInBounds( LInputHandler::instance()->curr_mouse_x, LInputHandler::instance()->curr_mouse_y ))           
            mBounds->selected = true;


        // selected
        if (mBounds->selected)
        {
            if (LInputHandler::instance()->mouse_down)
            {
                translateTo( mPosition.x + LInputHandler::instance()->mouse_delta_x, mPosition.y + LInputHandler::instance()->mouse_delta_y );
            }
        }
    }
}

int LEntity::debug_imgui_scene_update(FC_Font* font, SDL_Renderer* renderer, map<string, LEntity*> entities, int layerIdx)
{    
    // shift up function
    auto shifftUp = [](vector<LEntity*> &children, int j, int payload_j)
    {                        
        int idx = j;
        LEntity* curr = children[idx];
        LEntity* pay = children[payload_j];

        while ( idx >= payload_j )
        {
            children[idx] = pay;                                             
            pay = curr;                                                
            idx --;
            curr = children[idx];                            
        }
    };

    // shift down function
    auto shiftDown = [](vector<LEntity*> &children, int j, int payload_j)
    {
        int idx = j;
        LEntity* curr = children[idx];
        LEntity* pay = children[payload_j];

        while ( idx <= payload_j )
        {
            children[idx] = pay;                                             
            pay = curr;                                                
            idx ++;
            curr = children[idx];            
        }                        
    };

    string removeParentId = "";
    string removeChildId = "";    
    int selectedIndex = -1;             
            
    // loop through each entity in layer and display a nested box ( drag and drop )                                             
    for (int j=0; j<mChildren.size(); j++)
    {
        ImGui::PushID(j);        
        string name = mChildren[j]->mName + "_" + mChildren[j]->mid;
        
        // top selectable ( accept payload )
        if ( j == 0 )
        {
            ImGui::Selectable("", false, ImGuiSelectableFlags_Disabled, ImVec2{0, 0.25}); // Pad with space to extend size horizontally
            if (ImGui::BeginDragDropTarget())
            {
                const ImGuiPayload* test_payload = ImGui::GetDragDropPayload();
                if (test_payload != NULL && test_payload->IsDataType("EntityCell"))
                {
                    IM_ASSERT(test_payload->DataSize == sizeof(tuple<int, string, int, string>));
                    tuple<int, string, int, string> payload_j = *(const tuple<int, string, int, string>*)test_payload->Data;
                    // printf("layer index, child index: %i, %i\n", get<2>(payload_j), get<0>(payload_j));
                    if ( get<0>(payload_j) != 0 &&
                         findParent(get<1>(payload_j)) == NULL && 
                         get<2>(payload_j) == layerIdx &&
                         get<3>(payload_j) == mParentId )
                    {                        
                        if (ImGui::AcceptDragDropPayload("EntityCell"))
                        {
                            shiftDown(mChildren, j, get<0>(payload_j));
                        }                    
                    }
                }
                ImGui::EndDragDropTarget();
            }
        }

        // actual selectable                            
        int guiFlag = ImGuiTreeNodeFlags_NoTreePushOnOpen | ImGuiTreeNodeFlags_NoAutoOpenOnLog;
        
        // set selectable entity
        if (mSelectedEntity == mChildren[j]->mid)
        {            
            guiFlag |= ImGuiTreeNodeFlags_Selected;
        }
        // set leaf if child has no child 
        if (mChildren[j]->mChildren.size() == 0)
        {
            guiFlag |= ImGuiTreeNodeFlags_Bullet;
        }

        bool treeNode = ImGui::TreeNodeEx(name.c_str(),  guiFlag);

        bool popup = ImGui::BeginPopupContextItem("entity", ImGuiPopupFlags_MouseButtonRight);	
        if (popup)
        {
            if (ImGui::Selectable("delete")) 
            {
                mDeleteEntity = mChildren[j]->mid;	
            }
            ImGui::EndPopup();
        }        
        
        // set new selected entity
        if (mChildren[j]->mSelectedState != treeNode)
        {         
            mSelectedEntity = mChildren[j]->mid;
            mChildren[j]->mSelectedState = treeNode;
        }                

        // set payload: [int string int string]( child index, child id, layer index, parent id )
        if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_None))
        {
            // set new selected entity
            mSelectedEntity = mChildren[j]->mid;
            mChildren[j]->mSelectedState = treeNode;

            tuple<int, string, int, string> tup = make_tuple(j, mChildren[j]->mid, layerIdx, mParentId);
            ImGui::SetDragDropPayload("EntityCell", &tup, sizeof(tuple<int, string, int, string>)); // payload is tuple of int ( index ) and string ( mid )

            // Display preview (could be anything, e.g. when dragging an image we could decide to display
            // the filename and a small preview of the image, etc.)
            ImGui::Text("Moving %s", name.c_str());
            ImGui::EndDragDropSource();
        }

        // accept payload: this entity as child of target parent
        if (ImGui::BeginDragDropTarget())
        {
            const ImGuiPayload* test_payload = ImGui::GetDragDropPayload();
            if (test_payload != NULL && test_payload->IsDataType("EntityCell"))
            {
                IM_ASSERT(test_payload->DataSize == sizeof(tuple<int, string, int, string>));                                
                tuple<int, string, int, string> child_payload_mid = *(const tuple<int, string, int, string>*)test_payload->Data;
                // check if linking is allowable by checking if this entity's n-parent is the same as the payload
                if ( findParent(get<1>(child_payload_mid)) == NULL )
                {
                    if (ImGui::AcceptDragDropPayload("EntityCell"))
                    {
                        removeParentId = entities[get<1>(child_payload_mid)]->mParentId;                    
                                            
                        // remmove child from old parent
                        if (removeParentId != "")
                        {                        
                            entities[removeParentId]->unlink(entities[get<1>(child_payload_mid)]);
                        }

                        // set child to new parent
                        mChildren[j]->link(entities[get<1>(child_payload_mid)]);                        
                    }
                }
            }
           
            ImGui::EndDragDropTarget();
        }
    
        // update scene tree's children 
        if (treeNode)
        {
            selectedIndex = j;            
            ImGui::Indent();
            mChildren[j]->debug_imgui_scene_update(font, renderer, entities, layerIdx);
            ImGui::Unindent();                                                                       
        }        

        // bottom selectable
        ImGui::Selectable("", false, ImGuiSelectableFlags_Disabled, ImVec2{0, 0.25}); // Pad with space to extend size horizontally
        if (ImGui::BeginDragDropTarget())
        {
            const ImGuiPayload* test_payload = ImGui::GetDragDropPayload();
            if (test_payload != NULL && test_payload->IsDataType("EntityCell"))
            {
                IM_ASSERT(test_payload->DataSize == sizeof(tuple<int, string, int, string>));
                tuple<int, string, int, string> payload_j = *(const tuple<int, string, int, string>*)test_payload->Data;
                if ( findParent(get<1>(payload_j)) == NULL && get<3>(payload_j) == mParentId )
                {
                    if (get<2>(payload_j) == layerIdx && get<0>(payload_j) > j + 1) // if coming from bottom, shift everything down.
                    {                                                        
                        if (ImGui::AcceptDragDropPayload("EntityCell"))
                        {
                            shiftDown(mChildren, j + 1, get<0>(payload_j));
                        }
                    }
                    else if (get<2>(payload_j) == layerIdx && get<0>(payload_j) < j) // if comming from the top, shift everything up
                    {                    
                        if (ImGui::AcceptDragDropPayload("EntityCell"))
                        {
                            shifftUp(mChildren, j, get<0>(payload_j));
                        }
                    }
                }
            }
            ImGui::EndDragDropTarget();
        }
        ImGui::PopID();
    }         
    return selectedIndex;
}
