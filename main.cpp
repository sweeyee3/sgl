#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string>
#include <map>
#include <list>

#include "src/SGL_Texture.h"
#include "src/SGL_InputHandler.h"
#include "src/SGL_Constants.h"
#include "src/SGL_Bound.h"
#include "src/SGL_Loader.h"
#include "src/SGL_Transform.h"
#include "src/SGL_Timer.h"
#include "src/SGL_Entity.h"
#include "src/SGL_Scene.h"
#include "src/SDL_FontCache.h"

#include "DearImGui/imgui.h"
#include "imgui_sdl/imgui_sdl.h"
using namespace std;

// debug info display shortcut:
// backspace: enable / disable debug
// 1: mouse input handler
// 2: framerate
// 3: memory

bool init (); // start SDL and create window
bool load(); // loads media
void close(); // frees media and shuts down SDL
void update(); // update
void update_debug();
void update_imgui(); // update imgui
void render();

// global variables
bool gQuit = false;
SDL_Event* gEventInput;
SDL_Window* gWindow = NULL;

// scene
LScene* gScene;

// renderer
SDL_Renderer* gRenderer = NULL;

bool init()
{
	bool success = true;

	if ( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf(" SDK could not initialize! SDL_Error: %s\n", SDL_GetError());		
	}
	else
	{
		gWindow = SDL_CreateWindow( "SGL", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_RESIZABLE );		
		if ( gWindow == NULL )
		{
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{						
			//Create renderer for window
            gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED );
            if( gRenderer == NULL )
            {
                printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
                success = false;
            }
            else
            {
				gEventInput = new SDL_Event();
                //Initialize renderer color
                SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );

                //Initialize PNG loading
                int imgFlags = IMG_INIT_PNG;
                if( !( IMG_Init( imgFlags ) & imgFlags ) )
                {
                    printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
                    success = false;
                }

				 //Initialize SDL_ttf
                if( TTF_Init() == -1 )
                {
                    printf( "SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError() );
                    success = false;
                }

				// initialize Imgui
				ImGui::CreateContext();																	
				ImGuiSDL::Initialize(gRenderer, 800, 600);															

				// start timer
				LTimer::instance()->start();
            }			
		}		
	}

	return success;	
}

bool load()
{
	bool success = true;	

	/* LOAD ASSET FILES */

	// textures
	LResources::instance()->loadTextureFile("resources/img", gRenderer);	

	// font
	LResources::instance()->loadFontFile("resources/font", gRenderer);	

	/* LOAD META FILES */

	// textures
	LResources::instance()->loadMetaTextureFile("resources/img/img.mtex", gRenderer);	

	/* LOAD GAME ENTITY */

	// scene
	gScene = new LScene();
	if (!LResources::instance()->loadScene("sav/scene1.scene", gScene))
	{
		// load default scene		
		LEntity* root = gScene->instantiate("");

		// if no scene file to deserialize, create default
		gScene->rootId = root->mid;            
		gScene->createLayer();
	}
	gScene->entities[gScene->rootId]->mChildren[gScene->currentLayerIndex]->mSelectedState = true;   	
	// printf("number of layers: %lu\n", gScene->entities[gScene->rootId]->mChildren.size());

    return success;
}

void close()
{
	/* SAVE */
	LResources::instance()->saveScene(gScene->serialize(), "sav");

	/* DESTROY GAME ENTITY */	

	// destroy scene
	delete gScene;
	gScene = NULL;				

	// destroy resources
	LResources::instance()->free();	

	// destroy renderer
	SDL_DestroyRenderer( gRenderer );
	gRenderer = NULL;		

	/* DESTROY GAME SYSTEM */

	// destroy imgui
	ImGuiSDL::Deinitialize();
	ImGui::DestroyContext();

	delete gEventInput;
	gEventInput = NULL;

	// destroy window
	SDL_DestroyWindow( gWindow );
	gWindow = NULL;

	// quit sdl sub systems
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}

/*
 UPDATE ORDER:
 - input
 - resources
 - timer
 - scene ( to be expanded )  
*/

void update()
{
	LInputHandler::instance()->update(gEventInput, &gQuit);
	LResources::instance()->update();	
	LTimer::instance()->update();
	gScene->update();	
}

/*
 UPDATE ORDER:
 - input
 - resources
 - timer
 - scene ( to be expanded )  
*/
void update_debug()
{
	LInputHandler::instance()->debug_update();	
	LResources::instance()->debug_update();	
	LTimer::instance()->debug_update();		
	gScene->debug_update();
}

/*
 UPDATE ORDER:
 - input
 - resources
 - timer
 - scene ( to be expanded )  
*/
void update_imgui()
{
	ImGui::NewFrame();	

	LInputHandler::instance()->debug_imgui_update();
	LResources::instance()->debug_imgui_update();
	LTimer::instance()->debug_imgui_update();
	gScene->debug_imgui_update(LResources::instance()->fontMap["resources/font/babyblocks.ttf"], gRenderer);	
}

/*
 RENDER ORDER:
 - clear
 - bg (to be expanded)
 - scene ( to be expanded )
 - game ui
 - debug ui
 - imgui ui
*/
void render()
{
	// clear screen 
	SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
	SDL_RenderClear( gRenderer );

	// render background
	SDL_Rect fillRect = { 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT };
	SDL_SetRenderDrawColor( gRenderer, 0x00, 0x00, 0x00, 0xFF );        
	SDL_RenderFillRect( gRenderer, &fillRect );		

	// render scene
	gScene->render(gRenderer);
	gScene->debug_render(LResources::instance()->fontMap["resources/font/babyblocks.ttf"], gRenderer);	

	// render game ui
	

	// render debug ui
	switch (LInputHandler::instance()->debug_display_keycode)
	{
		case SDLK_1:
		LTimer::instance()->debug_render(LResources::instance()->fontMap["resources/font/babyblocks.ttf"], gRenderer);
		break;
		case SDLK_2:
		LInputHandler::instance()->debug_render(LResources::instance()->fontMap["resources/font/babyblocks.ttf"], gRenderer); // input handler debug info	
		break;
	}	
	
	// update imgui ui
	ImGui::Render();
	ImGuiSDL::Render(ImGui::GetDrawData());	
	
	SDL_RenderPresent( gRenderer );	
}

int main(int argc, char* args[]) 
{
	if (!init())
	{
		printf( " Failed to initialize!\n" );
	}
	else
	{
		if ( !load() )
		{
			printf("failed to load!\n");			
		}
		else
		{												
			while (!gQuit) // game loop
			{				
				// update here					
				update();

				// update debug here
				update_debug();

				// update imgui here
				update_imgui();				
				
				// render here
				render();
			}						
		}		
	}
	close();
	return 0;	
}